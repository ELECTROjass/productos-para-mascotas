import 'package:flutter/material.dart';

class CatHomePage extends StatelessWidget {
  const CatHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 243, 189, 10),
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Envío gratis en compras mayores a 80.000 ",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 243, 142, 10),
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(width: 10),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/5904/5904059.png",
                      height: 130,
                    ),
                    const Text(
                      "Gatos",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://www.agrocampo.com.co/media/catalog/product/cache/d51e0dc10c379a6229d70d752fc46d83/1/1/111110390-min.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Comida para gatos \n         Cat chow",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Center(
                  child: Column(
                    children: [
                      const Icon(
                        Icons.shopping_bag_rounded,
                        color: Colors.red,
                        size: 74.0,
                      ),
                      categoria("Alimento"),
                      categoria("Ropa"),
                      categoria("Accesorios"),
                      categoria("Salud"),
                      categoria("Juguetes"),
                      categoria("Arenas"),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/145/145812.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                            height: 70,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/3955/3955024.png",
                            width: 40,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/174/174883.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/5968/5968764.png",
                            width: 40,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://lh3.googleusercontent.com/YHnAR3AApaDdD-kBxA_KTnENOxyKxT4bR6PCIY1FAqgwux829z_hO7beE_00b5qfGrjGrGWNGN-OtpzvMGcaiOEEOkYru3-n4vIRQM75b4yErSyINw",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Arena para gatos \n        Whiskas",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://production-img.croper.com/Images/oSopvpToxqwAAwl0YQWeM.png",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 40.0),
                      child: Text(
                        "Arena para gatos \n        Gasty",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://supermercadosla80.com/rails/active_storage/representations/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBMUZla0E9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--21198558f3667047fdef8c54b1bca3b1e9b2800f/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9TY21WemFYcGxYM1J2WDJacGRGc0hhUUlnQTJrQ0lBTTZER052Ym5abGNuUkpJZ2hxY0djR09nWkZWRG9LYzJGMlpYSjdDRG9NY1hWaGJHbDBlV2xmT2dwemRISnBjRlE2RDJKaFkydG5jbTkxYm1SYkNHa0IvMmtCLzJrQi93PT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--f9b6205701877dad933126b17f2a1a57fbbba520/7702084057125.jpg?locale=es",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(40.0, 10.0, 0.0, 0.0),
                      child: Text(
                        "Arena para gatos \n        Don kat",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Center categoria(String nombre) {
    return Center(
      child: TextButton(
        onPressed: () {},
        // ignore: sort_child_properties_last
        child: Text(
          nombre,
          style: const TextStyle(color: Colors.white, fontSize: 25),
        ),
        style: TextButton.styleFrom(
            backgroundColor: const Color.fromARGB(49, 235, 157, 124),
            padding: const EdgeInsets.all(10.0),
            shape: const StadiumBorder()),
      ),
    );
  }
}
