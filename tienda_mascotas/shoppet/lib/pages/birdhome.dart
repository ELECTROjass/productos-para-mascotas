import 'package:flutter/material.dart';

class BirdHomePage extends StatelessWidget {
  const BirdHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 243, 189, 10),
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Envío gratis en compras mayores a 80.000 ",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 243, 142, 10),
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(width: 10),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/2469/2469704.png",
                      height: 130,
                    ),
                    const Text(
                      "Aves",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 50.0),
                      child: Image.network(
                        "https://exitocol.vtexassets.com/arquivos/ids/5418278-800-auto?v=637443150670600000&width=800&height=auto&aspect=true",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Comida para aves \n         Vitagrano",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Center(
                  child: Column(
                    children: [
                      const Icon(
                        Icons.shopping_bag_rounded,
                        color: Colors.red,
                        size: 74.0,
                      ),
                      categoria("Alimento"),
                      categoria("Ropa"),
                      categoria("Accesorios"),
                      categoria("Salud"),
                      categoria("Juguetes"),
                      categoria("Arenas"),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/145/145812.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                            height: 70,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/3955/3955024.png",
                            width: 40,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/174/174883.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/5968/5968764.png",
                            width: 40,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://www.pajareras.es/image/cache/catalog/Articulos/939-600x600.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Comida para aves \n            Jarad",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://shop-cdn-m.mediazs.com/bilder/versele/laga/prestige/premium/arena/para/pjaros/5/800/21282_pla_versele_vogelsand_marine5kg_5.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 40.0),
                      child: Text(
                        "Arena para aves \n        Prestige",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://www.perroperro.es/pics/2020/08/25/cococity-dispensador-de-agua-para-pajaros-y-loros-dispositivo-de-alimentacion-automatica-para-periquitos-periquitos-cacatuas-88049.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(40.0, 10.0, 0.0, 0.0),
                      child: Text(
                        "Bebedero para ave \n            Kesoto",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Center categoria(String nombre) {
    return Center(
      child: TextButton(
        onPressed: () {},
        // ignore: sort_child_properties_last
        child: Text(
          nombre,
          style: const TextStyle(color: Colors.white, fontSize: 25),
        ),
        style: TextButton.styleFrom(
            backgroundColor: const Color.fromARGB(49, 235, 157, 124),
            padding: const EdgeInsets.all(10.0),
            shape: const StadiumBorder()),
      ),
    );
  }
}
