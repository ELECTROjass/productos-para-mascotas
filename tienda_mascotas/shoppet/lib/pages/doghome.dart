import 'package:flutter/material.dart';

class DogHomePage extends StatelessWidget {
  const DogHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 243, 189, 10),
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Envío gratis en compras mayores a 80.000 ",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 243, 142, 10),
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(width: 10),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/4644/4644948.png",
                      height: 130,
                    ),
                    const Text(
                      "Perros",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://www.agrocampo.com.co/media/catalog/product/cache/d51e0dc10c379a6229d70d752fc46d83/1/1/111101688_ed-min.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 50.0),
                      child: const Text(
                        "Comida para perro \n         Dog chow",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Center(
                  child: Column(
                    children: [
                      const Icon(
                        Icons.shopping_bag_rounded,
                        color: Colors.red,
                        size: 74.0,
                      ),
                      categoria("Alimento"),
                      categoria("Ropa"),
                      categoria("Accesorios"),
                      categoria("Salud"),
                      categoria("Juguetes"),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/145/145812.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                            height: 70,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/3955/3955024.png",
                            width: 40,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/174/174883.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/5968/5968764.png",
                            width: 40,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://lh3.googleusercontent.com/HRDmMxoGk-6mROrBZ63LTRJVmbEkr3VNcU5EDLZkwn1bnQ8YJNedMaQ_OjBlellvPtcSJ0srGttP_EjtOm-x3DsyZ0kG7AFo60mXWp1b_3Elb7jE",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Arena para perros \n        Pedigree",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://cf.shopee.com.co/file/40a93ec7535edc81d44aadea43f23cf1",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 40.0),
                      child: Text(
                        "Arnes para perro \n           Rizer",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://www.sashija.com/wp-content/uploads/2021/09/SH-MAS000055N-3-CORREA-ROJO.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(40.0, 10.0, 0.0, 0.0),
                      child: Text(
                        "Correa retráctil 5m, \n          Sashija",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Center categoria(String nombre) {
    return Center(
      child: TextButton(
        onPressed: () {},
        // ignore: sort_child_properties_last
        child: Text(
          nombre,
          style: const TextStyle(color: Colors.white, fontSize: 25),
        ),
        style: TextButton.styleFrom(
            backgroundColor: const Color.fromARGB(49, 235, 157, 124),
            padding: const EdgeInsets.all(10.0),
            shape: const StadiumBorder()),
      ),
    );
  }
}
