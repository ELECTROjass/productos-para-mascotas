import 'package:flutter/material.dart';

class RoedoresHomePage extends StatelessWidget {
  const RoedoresHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 243, 189, 10),
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Envío gratis en compras mayores a 80.000 ",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 243, 142, 10),
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(width: 10),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/616/616453.png",
                      height: 130,
                    ),
                    const Text(
                      "Roedores",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 50.0),
                      child: Image.network(
                        "https://s.cornershopapp.com/product-images/2973472.jpg?versionId=oUIWTjhHlJ1PiML6kLWT8NGmkDUuCHz.",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Comida para hásmster \n         piamontina",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Center(
                  child: Column(
                    children: [
                      const Icon(
                        Icons.shopping_bag_rounded,
                        color: Colors.red,
                        size: 74.0,
                      ),
                      categoria("Alimento"),
                      categoria("Ropa"),
                      categoria("Accesorios"),
                      categoria("Salud"),
                      categoria("Juguetes"),
                      categoria("Arenas"),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/145/145812.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                            height: 70,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/3955/3955024.png",
                            width: 40,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/174/174883.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/5968/5968764.png",
                            width: 40,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://colombianrabbits.com/wp-content/uploads/2019/11/Ba%C3%B1o-arena-para-conejos-1.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Arena para hámsters \n            Rabbits",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://verdecora.es/1068554-large_default/alimento-hamster-1-kilo-verdecora.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 40.0),
                      child: Text(
                        "Comida para hámster \n        Verdecora",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://http2.mlstatic.com/D_NQ_NP_877330-MLA47121502430_082021-O.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(40.0, 10.0, 0.0, 0.0),
                      child: Text(
                        "Arenero para hámster \n            Ace pet",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Center categoria(String nombre) {
    return Center(
      child: TextButton(
        onPressed: () {},
        // ignore: sort_child_properties_last
        child: Text(
          nombre,
          style: const TextStyle(color: Colors.white, fontSize: 25),
        ),
        style: TextButton.styleFrom(
            backgroundColor: const Color.fromARGB(49, 235, 157, 124),
            padding: const EdgeInsets.all(10.0),
            shape: const StadiumBorder()),
      ),
    );
  }
}
