import 'package:flutter/material.dart';

class FishHomePage extends StatelessWidget {
  const FishHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 243, 189, 10),
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Envío gratis en compras mayores a 80.000 ",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 243, 142, 10),
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                const SizedBox(width: 10),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/5783/5783032.png",
                      height: 130,
                    ),
                    const Text(
                      "Peces",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 60.0),
                      child: Image.network(
                        "https://http2.mlstatic.com/D_NQ_NP_939901-MCO41448815561_042020-O.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 60.0),
                      child: const Text(
                        "Comida para peces \n          Nutripez",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Center(
                  child: Column(
                    children: [
                      const Icon(
                        Icons.shopping_bag_rounded,
                        color: Colors.red,
                        size: 74.0,
                      ),
                      categoria("Alimento"),
                      categoria("Accesorios"),
                      categoria("Salud"),
                      categoria("Juguetes"),
                      categoria("Arenas"),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/145/145812.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                            height: 70,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/3955/3955024.png",
                            width: 40,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/174/174883.png",
                            width: 40,
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          Image.network(
                            "https://cdn-icons-png.flaticon.com/512/5968/5968764.png",
                            width: 40,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://images-na.ssl-images-amazon.com/images/I/81LeA2X4iqL._AC._SR360,460.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    // ignore: prefer_const_constructors
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: const Text(
                        "Comida para peces \n            Tetra",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "https://i.linio.com/p/f6a07c8d73d5b7a0e6fdf708c758430f-product.webp",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 40.0),
                      child: Text(
                        "Dispensador de comida \n        NikoKassem",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: Image.network(
                        "http://4.bp.blogspot.com/_Voh2SiKy5LA/TNb9NvUgOOI/AAAAAAAAAEo/GExrqqX-md8/w1200-h630-p-k-no-nu/sal+halita350g.jpg",
                        height: 120,
                        width: 130,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.fromLTRB(40.0, 10.0, 0.0, 0.0),
                      child: Text(
                        "Sal para acuario \n          Halita",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Center categoria(String nombre) {
    return Center(
      child: TextButton(
        onPressed: () {},
        // ignore: sort_child_properties_last
        child: Text(
          nombre,
          style: const TextStyle(color: Colors.white, fontSize: 25),
        ),
        style: TextButton.styleFrom(
            backgroundColor: const Color.fromARGB(49, 235, 157, 124),
            padding: const EdgeInsets.all(10.0),
            shape: const StadiumBorder()),
      ),
    );
  }
}
