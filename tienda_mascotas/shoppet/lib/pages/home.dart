import 'package:flutter/material.dart';

class ApplicationApp extends StatelessWidget {
  const ApplicationApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Hola mundo",
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 243, 189, 10),
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Envío gratis en compras mayores a 80.000 ",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            )),
      ),
      body: Center(
        child: Column(
          children: [
            const SizedBox(height: 40),
            const Text("Selecciona el tipo de tu mascota",
                style: TextStyle(
                    fontSize: 34,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
                textAlign: TextAlign.center),
            const SizedBox(
              height: 40,
            ),
            Row(
              children: [
                const SizedBox(width: 40),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/5904/5904059.png",
                      height: 150,
                    ),
                    const Text(
                      "Gatos",
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
                const SizedBox(width: 40),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/4644/4644948.png",
                      height: 150,
                    ),
                    const Text(
                      "Perros",
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    )
                  ],
                )
              ],
            ),
            Row(
              children: [
                const SizedBox(width: 30),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/616/616453.png",
                      height: 150,
                    ),
                    const Text(
                      "Roedores",
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
                const SizedBox(width: 30),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/2469/2469704.png",
                      height: 150,
                    ),
                    const Text(
                      "Aves",
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    )
                  ],
                )
              ],
            ),
            Row(
              children: [
                const SizedBox(width: 140),
                Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/512/5783/5783032.png",
                      height: 150,
                    ),
                    const Text(
                      "Peces",
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.cyan),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
