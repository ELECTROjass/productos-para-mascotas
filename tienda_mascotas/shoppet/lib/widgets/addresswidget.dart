import 'package:flutter/material.dart';

class Addresswidget extends StatelessWidget {
  const Addresswidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: const [
            Text(
              "Laguna Guatavita",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              "Guatavita-cundinamarca",
              style: TextStyle(
                color: Color.fromRGBO(150, 150, 150, 1.0),
              ),
            )
          ],
        ),
        const SizedBox(
          width: 120,
        ),
        const Icon(
          Icons.star,
          color: Colors.red,
        ),
        const Text("41")
      ],
    );
  }
}
