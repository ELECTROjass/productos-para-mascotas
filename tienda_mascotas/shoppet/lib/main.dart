import 'package:flutter/material.dart';
import 'package:shoppet/pages/roedoreshome.dart';

void main(List<String> args) {
  runApp(const ApplicationApp());
}

class ApplicationApp extends StatelessWidget {
  const ApplicationApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          color: Colors.black,
          child: const RoedoresHomePage(),
        ),
      ),
    );
  }
}
