# Productos Para Mascotas

App de una tienda virtual para comprar cosas de mascotas.

# Integrantes

Taylor Franco Grisales
Oscar Nupan
Danny Ariza Guaqueta
John Fredy Campaz
David Rincón

# Roles del proyecto

- Tripulante: Gestor del proyecto, Taylor Franco
- Tripulante: Gestor BD, Oscar Nupan
- Tripulante: Desarrollador frontend, Danny Ariza
- Tripulante: Desarrollador backend, Jhon Fredy
- Tripulante: Tester, David Rincón
